import { createRequest } from './req'

const apiUrl = 'http://localhost:3017/api'

const request = createRequest(apiUrl)

export const api = {
  abonents: {
    getList: () => request({ url: '/abonents' }),
    get: (id: number) => request({ url: `/abonents/${id}` }),
    add: (body: any) => request({ url: '/abonents', method: 'POST', body }),
    edit: (id: number, body: any) =>
      request({ url: `/abonents/${id}`, method: 'PUT', body }),
    delete: (id: number) =>
      request({ url: `/abonents/${id}`, method: 'DELETE' }),
  },
  plans: {
    getList: () => request({ url: '/plans' }),
    get: (id: number) => request({ url: `/plans/${id}` }),
    add: (body: any) => request({ url: '/plans', method: 'POST', body }),
    edit: (id: number, body: any) =>
      request({ url: `/plans/${id}`, method: 'PUT', body }),
    delete: (id: number) => request({ url: `/plans/${id}`, method: 'DELETE' }),
  },
}
